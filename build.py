#!/usr/bin/python3

import json
import os
import pathlib
import shutil
import subprocess
import sys
import yaml

config = yaml.safe_load(pathlib.Path(sys.argv[1]).read_text("UTF-8"))

config.update(
    {
        "package_name": f"{config['provider']}-{config['app_slug']}_{config['version']}",
        "app_home": f"/var/opt/lib/{config['provider']}/{config['app_slug']}/home",
        "container_run": f"/var/opt/run/{config['provider']}/{config['app_slug']}/containers",
        "container_home": f"/var/opt/lib/{config['provider']}/{config['app_slug']}/containers",
        "app_location": f"/opt/{config['provider']}/{config['app_slug']}",
    }
)

config.update(
    {
        "compose_file": f"{config['app_location']}/compose/{config['app_slug']}.yml",
        "compose_project": f"{config['provider']}-{config['app_slug']}",
        "system_user": f"{config['provider']}-{config['app_slug']}".replace(".", "_"),
    }
)

pathlib.Path(sys.argv[2], "cookiecutter.json").write_text(
    json.dumps(config), encoding="UTF-8"
)

cookiecutter_executable = pathlib.Path(os.environ["HOME"], ".local/bin/cookiecutter")
if not cookiecutter_executable.is_file():
    subprocess.run(
        ["python3", "-m", "pip", "install", "--user", "cookiecutter"], check=True
    )

subprocess.run(
    [str(cookiecutter_executable), "-f", "--no-input", sys.argv[2]], check=True
)

project_repo_path = pathlib.Path(__file__).parents[1]
build_dir = f"{config['compose_project']}_{config['version']}"
compose_dir = project_repo_path.joinpath(
    build_dir, f"opt/{config['provider']}/{config['app_slug']}/compose"
)
compose_dir.mkdir()

shutil.copy(
    project_repo_path.joinpath("debian/changelog"),
    project_repo_path.joinpath(f"{build_dir}/debian"),
)

shutil.copy(
    project_repo_path.joinpath(f"{config['app_slug']}.yml"),
    compose_dir,
)

subprocess.run(
    ["/usr/bin/dpkg-buildpackage", "-uc", "-us", "-b"],
    cwd=project_repo_path.joinpath(build_dir),
    check=True,
)

artifact_directory = pathlib.Path(os.environ["HOME"], config["artifact_directory"])
artifact_directory.mkdir(parents=True, exist_ok=True)
package_file_name = f"{str(build_dir)}_all.deb"
project_repo_path.joinpath(package_file_name).rename(
    artifact_directory.joinpath(package_file_name)
)

subprocess.run(  # nosec
    [
        "ansible-playbook",
        pathlib.Path(__file__).parent.joinpath(config["upload_playbook"]),
        "-e",
        f"artifact={artifact_directory.joinpath(package_file_name).absolute()}",
    ],
    check=True,
    cwd=str(artifact_directory.absolute()),
)
